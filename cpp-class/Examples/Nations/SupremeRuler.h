#ifndef SUPREME_RULER_DEFINED
#define SUPREME_RULER_DEFINED

#include <string>

class Nation;

class SupremeRuler
{
public:
	SupremeRuler(const std::string& nm);

	void seize_power(Nation* n);
	void pass_decree();
	std::string get_name();

private:
	std::string name;
	Nation* nation;
};

#endif