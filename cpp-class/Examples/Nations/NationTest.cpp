#include "Nation.h"
#include "SupremeRuler.h"

void some_function(Nation* n)
{
	SupremeRuler* despot = n->get_ruler();

	despot->pass_decree();
}

int main()
{
	Nation nation("Ruritania");
	SupremeRuler ruler("Rudolf Rassendy");

	nation.elect(&ruler);

	some_function(&nation);

	return 0;
}

