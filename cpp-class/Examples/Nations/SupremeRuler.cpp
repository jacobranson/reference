#include <iostream>
#include "SupremeRuler.h"
#include "Nation.h"

SupremeRuler::SupremeRuler(const std::string& nm)
:name(nm), nation(nullptr)
{}

void SupremeRuler::seize_power(Nation* n)
{
	if (nation != n)
	{
		nation = n;
		nation->elect(this);
	}
}

void SupremeRuler::pass_decree()
{
	std::cout << "Obey me!\n";

	if (nation != nullptr)
		nation->take_orders();
}

std::string SupremeRuler::get_name()
{
	return name;
}
