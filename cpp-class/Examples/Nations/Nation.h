#ifndef NATION_INCLUDED
#define NATION_INCLUDED

#include<string>

class SupremeRuler;

class Nation
{
public:
	Nation(const std::string& nm);

	void elect(SupremeRuler* r);
	void take_orders();

	SupremeRuler* get_ruler();

private:
	std::string name;
	SupremeRuler* ruler;
};

#endif

