#include <iostream>
#include "Nation.h"
#include "SupremeRuler.h"

Nation::Nation(const std::string& nm)
:name(nm),ruler(nullptr)
{}

void Nation::elect(SupremeRuler* r)
{
	if (ruler != r)
	{
		ruler = r;
		r->seize_power(this);
	}
}

void Nation::take_orders()
{
	std::cout << "We obey!\n";
}

SupremeRuler* Nation::get_ruler()
{
	return ruler;
}

