#ifndef POINT_INCLUDED
#define POINT_INCLUDED
#include <iostream>

class Point
{
	friend std::ostream& operator<<(std::ostream& os, const Point& p);
  public:
    Point();
    Point(int i, int j);
    void move(int i, int j);
    int get_x() const;
    int get_y() const;
    void clear();
	bool operator==(const Point& op) const;
  private:
    int x, y;
};
#endif
