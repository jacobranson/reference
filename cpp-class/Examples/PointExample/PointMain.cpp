#include <iostream>
#include <iomanip>
#include <string>
#include "point.h"
using namespace std;

void main()
{
	Point p1;
	cout << p1 << endl;
	Point p2(10,22);
	cout << p2 << endl;
	p1.move(10,22);
	if (p1 == p2) cout << "Points are equal\n";
}
