#include <iostream>
#include "point.h"
using namespace std;

// Friends

ostream& operator<<(ostream& os, const Point& p)
{
    os << '(' << p.x << ',' << p.y << ')';
    return os;
}

// Members

Point::Point() { x = 0; y = 0; }

Point::Point(int x, int y) { this->x = x; this->y = y; }

void Point::move(int x, int y) { this->x = x; this->y = y; }

int Point::get_x() const { return x; }

int Point::get_y() const { return y; }

void Point::clear() { x = 0; y = 0; }

bool Point::operator==(const Point& p) const
{
    if (x == p.x && y == p.y) return true;
    return false;
}
