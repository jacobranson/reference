// Templates.cpp : Defines the entry point for the console application.
//
#include <string>
#include <iostream>
#include "maximum.h"

using namespace std;

int main()
{
	cout << maximum(3, 7) << "\n";
	cout << maximum(3.8, 1.4) << "\n";

	string s1("hi"), s2("bye");

	cout << maximum(s1, s2) << "\n";
	//cout << maximum(3, 3.5) << "\n";
	cout << maximum<double>(3, 3.5) << "\n";

	return 0;
}

