#include <iostream>
#include <string>

int main()
{
    bool b;   char c;   int i;   double x;

    c = 97;				// c gets ____________
	std::cout << c << "\n";
    i = c + 3;			// i gets ____________
	std::cout << i << "\n";
    b = -1000;			// b gets ____________
	std::cout << b << "\n";
    i = i + b;			// i gets ____________
	std::cout << i << "\n";
    i = 5.8;			// i gets ____________
	std::cout << i << "\n";
    x = i * 2.5;		// x gets ____________
	std::cout << x << "\n";
    x = i / 2 * 2.0;	// x gets ____________
	std::cout << x << "\n";
}
