// LambdaSorting.cpp : Defines the entry point for the console application.
//
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

int main()
{
	vector<string> nlst;
	nlst.push_back("Jasper"); nlst.push_back("Richard");
	nlst.push_back("Ulf"); nlst.push_back("Ali");
	nlst.push_back("Neil"); nlst.push_back("Mike");

	std::sort(nlst.begin(), nlst.end(),
		[](string a, string b) {return b < a; });

	for (auto str : nlst)
		cout << str << "\n";

	return 0;
}
