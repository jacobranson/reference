#include <iostream>
#include <string>
using namespace std;

void val_and_ref(string s1, string& s2)
{
    s1.append(" Adams");
    s2.append(" Kent");
}

int main()
{
    string te1 = "Greg", te2 = "Jasper";
    cout << "before val_and_ref:\n"
         << te1 << ", " << te2 << "\n";
    val_and_ref(te1, te2);
    cout << "after val_and_ref:\n"
         << te1 << ", " << te2 << "\n";

    return 0;
}
