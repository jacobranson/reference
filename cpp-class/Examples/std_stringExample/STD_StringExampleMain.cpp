#include <iostream>
#include <string>
using namespace std;

int main()
{
	string s1;					// s1 is initially empty
    string s2 = "hello";		// s2 initialized to "hello"
    s1 = s2 + ", world";		// + operation means concatenate
    cout << s1 << "\n";
    cout << "s2 is " << s2.length() << " chars long.\n";
	s1.insert(5, " again");		// Insert after 5th char
	cout << s1 << "\n";

	return 0;
} 
