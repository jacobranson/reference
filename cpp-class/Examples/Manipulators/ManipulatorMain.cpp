#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main()
{
    cout << "Half of $" << 3 << " is $"
         << fixed             // Display in fixed decimal format...
         << setprecision(2)   // ... with two digits after the decimal
         << 1.5 << "\n\n";

    cout << setw(5) << "hey"  // hey in 5-character wide field
         << "hi"              // hi in default width!
         << setw(5) << "ha"   // ha in 5-character wide field
         << "\n";
}
