#include <iostream>
#include "Employee.h"

class Manager : public Employee
{
  public:
    Manager(std::string n, int i, double s);
    void set_budget(double b);
    double get_budget() const;
    void print_budget() const;
	virtual void print(std::ostream& ) const;
  private:
    double budget;
};
