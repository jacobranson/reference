#include <iostream>
#include <string>

class Employee
{
	friend std::ostream& operator<<(std::ostream& os, const Employee& e);
  public:
    Employee(std::string n, int i, double s);
	std::string get_name() const;
	void change_name(std::string n);
    int get_id() const;
    double get_salary() const;
    void change_salary(double s);
	virtual void print(std::ostream& os) const;
  private:
    Employee(const Employee& e);
	Employee& operator=(const Employee& e);

	std::string name;
    const int id;
    double salary;
};
