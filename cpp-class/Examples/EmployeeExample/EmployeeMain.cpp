#include <iostream>
#include <iomanip>
#include "CEO.h"

using namespace std;

void main()
{
	Employee fred("Fred Flintstone",1221,23.11);
	cout << fred << endl;

	Manager judy("Judy Jetson",22,12112321.12);
	judy.set_budget(9999.99);
	cout << judy << endl;

	// Slide 7-28

    Employee john("John Karl", 123, 30000.0);
    Employee paul("Paul Choi", 201, 31500.0);

    Manager susan("Susan Todd", 100, 44000.0);
    susan.set_budget(10000.);
    CEO andrea("Andrea O'Brien", 8, 62000.0);
    andrea.set_budget(250000.);

    Employee* company[] = { &paul, &john, &andrea, &susan };
    int nemp = sizeof(company) / sizeof(Employee*);

    cout << "Company employees are:\n";
    for (int i = 0; i < nemp; ++i)
		cout << *company[i] << "\n";
}
