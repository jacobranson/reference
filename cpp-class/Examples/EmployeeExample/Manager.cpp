#include "manager.h"
using namespace std;

Manager::Manager(string n, int i, double s) : Employee(n, i, s), budget(0.0) {}

void Manager::set_budget(double b)
{
	budget = b;
}

double Manager::get_budget() const
{
	return budget;
}

void Manager::print(ostream& os) const
{
	Employee::print(os);
	os << ", Budget: " << budget;
}
