#include <iostream>
#include <iomanip>
#include <string>
#include "employee.h"
using namespace std;

Employee::Employee(string n, int i, double s) : name(n), id(i), salary(s) {}

std::string Employee::get_name() const { return name; }
void Employee::change_name(string n) { name = n; }

int Employee::get_id() const { return id; }

double Employee::get_salary() const { return salary; }
void Employee::change_salary(double s) { salary = s; }

void Employee::print(ostream& os) const
{
    os << "Name: " << name << ", ID: " << id;
	os << ", Salary: " << fixed << setprecision(2) << salary;
}

ostream& operator<<(ostream& os, const Employee& e)
{
	e.print(os);
	return os;
}
