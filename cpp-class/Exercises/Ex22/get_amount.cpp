#include <iostream>

double get_amount()				// Read amount from user.
{
	double amount;					// Local working variable.
	bool waiting_for_input(true);	// Control flag.
	while(waiting_for_input)		// Wait for input.
	{
		std::cout << "Input amount: ";		// Prompt.
		if (std::cin >> amount)		// Get input.
			waiting_for_input = false;
		else
		{
			std::cerr << "Invalid input, please try again\n";
			std::cin.clear();				// Clear error flags.
			std::cin.ignore(INT_MAX, '\n');	// Purge rest of input line.
		}
	}
	return amount;						// Return the amount read.
}
