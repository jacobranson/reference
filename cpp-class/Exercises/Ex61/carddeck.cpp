#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "carddeck.h"
#include "playingcard.h"
using namespace std;

// Add your definitions here






// Shuffle provided for you.

void CardDeck::shuffle()
{
	srand((unsigned int)time(nullptr));	// Seed random number from the clock
	random_shuffle(cards.begin(), cards.end());
}

// Print provided for you

void CardDeck::print() const
{
	int i = 0;
	for (auto card : cards)
	{
		if (((i % 13) == 0) && (i != 0)) cout << '\n';
		card->print();
		++i;
	}
	cout << "\n";
}
