#include <iostream>
#include "facecard.h"
using namespace std;

FaceCard::FaceCard(int rank, int suit)
	:PlayingCard(rank, suit)
{
	if (rank == KING && suit == DIAMONDS) eyes = 1;
	else if (rank == JACK && suit == HEARTS) eyes = 1;
	else if (rank == JACK && suit == SPADES) eyes = 1;
}

PlayingCard* FaceCard::clone() const
{
	return new FaceCard(*this);
}


void FaceCard::print()const
{
	if (is_face_up())
		cout << char(eyes);
	PlayingCard::print();
}