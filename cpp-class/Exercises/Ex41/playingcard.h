const int HEARTS   = 0;
const int DIAMONDS = 1;
const int CLUBS	   = 2;
const int SPADES   = 3;

const int JOKER =  0;
const int ACE   =  1;
const int JACK  = 11;
const int QUEEN = 12;
const int KING  = 13;

// Place your definition for the class PlayingCard here