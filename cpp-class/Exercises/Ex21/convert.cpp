#include "convert.h"

double convert(double d)			// Monetary conversion function.
{
	double converted_value;			// Resultant value.
	const double rate = 1.3033;		// Conversion rate
	converted_value = d / rate;		// Perform the conversion.
	return converted_value;			// Return the result
}
