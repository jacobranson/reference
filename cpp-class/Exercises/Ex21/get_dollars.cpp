#include <iostream>
#include "get_dollars.h"

double get_dollars()				// Read doller amount from user.
{
	double d;						// Local working variable.
	bool waiting_for_input(true);	// Control flag.
	while(waiting_for_input)		// Wait for input.
	{
		std::cout << "Input amount in dollars? ";		// Prompt.
		if (std::cin >> d) waiting_for_input = false;	// Get input.
		else
		{
			std::cerr << "Invalid input, please try again\n";
			std::cin.clear();				// Clear error flags.
			std::cin.ignore(INT_MAX, '\n');	// Purge rest of input line.
		}
	}
	return d;						// Return the amount read.
}

//Insert your overload here


