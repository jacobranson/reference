#include <iostream>
#include "playingcard.h"
using namespace std;

// Uncomment lines from the main program as directed in the exercise manual

int main()
{
	PlayingCard c1(3,HEARTS);		// Create the 3 of Hearts
	c1.flip();						// Now flip the card
	c1.print();

	PlayingCard c2(KING,CLUBS);
	c2.flip();
	c2.print();

	const PlayingCard ace(ACE,DIAMONDS);
//	ace.flip();
//	ace.print();

	return 0;
}
