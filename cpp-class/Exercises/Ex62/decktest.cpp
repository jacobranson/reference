#include <iostream>
#include "carddeck.h"
#include "gamehand.h"
using namespace std;

void test()
{
	CardDeck deck;			// Create a card deck
	cout << "Unshuffled...\n";
	deck.print();			// Print the deck
	deck.shuffle();			// Shuffle the deck
	cout << "Shuffled...\n";
	deck.print();			// Print it again

	//CardDeck deck2(deck);	// Construct one from another
	//cout << "Second deck should be the same\n";
	//deck2.print();			// Display it.
}

int main()
{
	try { test(); }
	catch(const char* message)
	{
		cout << message << "\n";
	}
	return 0;
}
