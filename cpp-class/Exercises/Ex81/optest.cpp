#include <iostream>
#include "facecard.h"
#include "carddeck.h"
#include "gamehand.h"
using namespace std;

int main()
{
	CardDeck deck;
	deck.shuffle();
	cout << "The deck is...\n";
	deck.print();

	GameHand you, sys;
	for (int i = 0; i < 2; i++)
	{
		PlayingCard* cp;
		cp = deck.deal();
		you.add(cp);
		cp = deck.deal();
		sys.add(cp);
	}
	cout << "You got a hand of value " << you.evaluate() << "\n";
	cout << "The system got a hand of value " << sys.evaluate() << "\n";

//	Add the win, loss, tie report here
	//if (you < sys)
	//	cout << "Sorry, you lost\n";
	//else if (you == sys)
	//	cout << "We tied\n";
	//else
	//	cout << "You won\n";

	return 0;
}
