#ifndef CARDDECK_INCLUDED
#define CARDDECK_INCLUDED

#include <vector>
class PlayingCard;

class CardDeck
{
public:
	CardDeck();
	CardDeck(const CardDeck&);
	~CardDeck();
	PlayingCard* deal();
	void add(PlayingCard* cp);
	void print() const;	// provided
	void shuffle();		// provided
private:
	std::vector<PlayingCard*> cards;
};

#endif
