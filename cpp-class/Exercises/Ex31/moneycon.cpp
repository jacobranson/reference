// Moneycon.cpp : Defines the entry point for the console application.
#include <iostream>
#include <string>
#include "get_amount.h"
using namespace std;

int main()
{
	double amount = get_amount();
	// TODO: create output file stream here

	cout << "Converted is: " << amount << "\n";

	double rates[] = { 1.3033, 1.4968, 135.101, 9.528, 0.7382 };
	string codes[] = { "USD", "CAD", "JPY", "SEK", "GBP" };

	for (int i = 0; i < sizeof(rates) / sizeof(rates[0]); ++i)
		cout << codes[i] << ": " << amount * rates[i] << "\n";

	return 0;
}
