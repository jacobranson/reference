//	File moneycon.cpp

#include <iostream>

double get_dollars();						// Declare functions
double convert(double d);

int main()									// Entry point (called by system)
{
	double euro, dollar;					// Define variables.

	dollar = get_dollars();					// Get a dollar amount.
	euro = convert(dollar);					// Convert to euros.
	std::cout << dollar << " dollars is "	// Output the result.
			  << euro << " euros.\n";
	return 0;								// Return to the system.
}

double convert(double d)					// Monetary conversion function.
{
	double converted_value;					// Resultant value.
	const double rate = 1.3033;				// Conversion rate Sep, 2015
	converted_value = d / rate;				// Perform the conversion.
	return converted_value;					// Return the result
}

double get_dollars()						// Read doller amount from user.
{
	double d;								// Local working variable.
	bool waiting_for_input(true);			// Control flag.
	while(waiting_for_input)				// Wait for input.
	{
		std::cout << "Input amount in dollars? ";		// Prompt.
		if (std::cin >> d) waiting_for_input = false;	// Get input.
		else
		{
			std::cerr << "Invalid input, please try again\n";
			std::cin.clear();				// Clear error flags.
			std::cin.ignore(INT_MAX, '\n');	// Purge rest of input line.
		}
	}
	return d;								// Return the amount read.
}
