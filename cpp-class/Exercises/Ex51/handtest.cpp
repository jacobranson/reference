#include <iostream>
#include "playingcard.h"
#include "gamehand.h"
using namespace std;

// Uncomment lines from play_cards() as directed in the exercise manual
void play_cards(PlayingCard& c1, PlayingCard& c2)
{
	cout << "GameHand testing not implemented yet\n";
	// create your GameHand object here and name it hand.
//	GameHand hand;


	// Add the cards c1 and c2 to the hand
//	hand.add(&c1);
//	hand.add(&c2);
//	cout << "The hand evaluates to " << hand.evaluate() << "\n";
	// Bonus material
//	cout << "The highest card is: ";
//	hand.get_highest_card()->print();
//	cout << "The lowest card is: ";
//	hand.get_lowest_card()->print();
}

int main()
{
	PlayingCard c1(3, SPADES);		// Create 1st card.
	PlayingCard c2(QUEEN, CLUBS);	// Create 2nd card.

	cout << "Card 1 is: "; c1.print();
	cout << "Card 2 is: "; c2.print();

	play_cards(c1, c2);

	PlayingCard c3(10, HEARTS);		// Create 3rd card.
	PlayingCard c4(QUEEN, DIAMONDS);	// Create 4th card.

	cout << "Card 3 is: "; c3.print();
	cout << "Card 4 is: "; c4.print();

	play_cards(c3, c4);

	return 0;
}
