#include <iostream>
#include "carddeck.h"
#include "facecard.h"

using namespace std;

int main()
{
	PlayingCard pc1(9,SPADES);		// regular PlayingCard
	cout << "Card c1 is ";
	pc1.print();
	cout << "\n";
	FaceCard fc1(QUEEN, SPADES);	// Two-eyed playing card
	cout << "Face card fc1 is two eyed ";
	fc1.print();
	cout << "\n";
	FaceCard fc2(JACK, SPADES);		// One-eyed playing card
	cout << "Face card fc2 is one eyed ";
	fc2.print();
	cout << "\n";

	CardDeck dk;
	dk.shuffle();
	cout << "Printing the deck\n"; dk.print();

	CardDeck dk2(dk);
	cout << "Printing the deck\n"; dk2.print();

	return 0;
}
