#ifndef PLAYINGCARD_INCLUDED
#define PLAYINGCARD_INCLUDED

const int HEARTS   = 0;
const int DIAMONDS = 1;
const int CLUBS	   = 2;
const int SPADES   = 3;

const int JOKER =  0;
const int ACE   =  1;
const int JACK  = 11;
const int QUEEN = 12;
const int KING  = 13;

// Place your PlayingCard class declaration here

class PlayingCard
{
public:
	PlayingCard(int rank, int suit);
	virtual ~PlayingCard() {}
	virtual void print() const;
	int get_rank() const;
	int get_suit() const;
	void flip();
	bool is_face_up() const;
	virtual PlayingCard* clone() const;

private:
	const int rank;
	const int suit;
	bool face_up = true;
};
#endif
