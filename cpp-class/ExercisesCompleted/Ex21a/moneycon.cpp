#include <iostream>
#include "get_dollars.h"
#include "convert.h"

int main()									// Entry point (called by system)
{
	auto dollar = get_dollars(100.0);		// Get a dollar amount.
	double euro = convert(dollar);			// Convert to euros.
	
	std::cout << dollar << " dollars is "	// Output the result.
		<< euro << " euros.\n";

	return 0;								// Return to the system.
}

