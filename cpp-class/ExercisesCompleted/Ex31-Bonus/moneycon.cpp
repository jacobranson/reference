// Moneycon.cpp : Defines the entry point for the console application.
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "get_amount.h"
using namespace std;

int main()
{
	string filename;
	cout << "Enter the name of the output file: ";
	getline(cin, filename);
	if (filename.length() == 0)
		filename = "convert.txt";
	double amount = get_amount();
	// TODO: create output file stream here
	ofstream fout(filename);

	fout << "Converted is: " << amount << "\n";

	vector<double> rates = { 1.3033, 1.4968, 135.101, 9.528, 0.7382 };
	vector<string> codes = { "USD", "CAD", "JPY", "SEK", "GBP" };

	for (int i = 0; i != rates.size(); ++i)
		fout << codes[i] << ": " << amount * rates[i] << "\n";

	return 0;
}
