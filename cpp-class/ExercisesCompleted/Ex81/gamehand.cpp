// Add your implementation of the game hand here
#include "gamehand.h"
#include "playingcard.h"

void GameHand::add(PlayingCard* cp)
{
	if (cp1 == nullptr)
		cp1 = cp;
	else
		cp2 = cp;
}

int GameHand::evaluate() const
{
	return evaluate_card(cp1) + evaluate_card(cp2);
}

PlayingCard * GameHand::get_highest_card() const
{
	if (evaluate_card(cp2) > evaluate_card(cp1))
		return cp2;
	return cp1;
}

PlayingCard * GameHand::get_lowest_card() const
{
	if (evaluate_card(cp1) < evaluate_card(cp2))
		return cp1;
	return cp2;
}

int GameHand::evaluate_card(PlayingCard* cp) const
{
	if (cp != nullptr)
	{
		int rank = cp->get_rank();
		if (rank >= JACK)
			return 10;
		if (rank == ACE)
			return 11;
		return rank;
	}
	return 0;
}

bool GameHand::operator<(const GameHand& other) const
{
	return evaluate() < other.evaluate();
}


bool GameHand::operator==(const GameHand& other) const
{
	return evaluate() == other.evaluate();
}
