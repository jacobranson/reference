#ifndef GAMEHAND_INCLUDED
#define GAMEHAND_INCLUDED

class PlayingCard;

class GameHand
{
public:
	void add(PlayingCard* cp);
	int evaluate() const;
private:
	int evaluate_card(PlayingCard* cp) const;
	PlayingCard* cp1 = nullptr;
	PlayingCard* cp2 = nullptr;
};

#endif
