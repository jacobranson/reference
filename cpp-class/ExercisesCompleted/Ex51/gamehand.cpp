#include "gamehand.h"
#include "playingcard.h"

void GameHand::add(PlayingCard* cp)
{
	if (cp1 == nullptr)
		cp1 = cp;
	else
		cp2 = cp;
}

int GameHand::evaluate() const
{
	return evaluate_card(cp1) + evaluate_card(cp2);
}

int GameHand::evaluate_card(PlayingCard* cp) const
{
	int value = 0;
	if (cp != nullptr)
		if (cp->get_rank() > 10)
			value = 10;
		else if (cp->get_rank() == ACE)
			value = 11;
		else
			value = cp->get_rank();
	return value;
}
