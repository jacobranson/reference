#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "carddeck.h"
#include "playingcard.h"
using namespace std;

// Add your definitions here
CardDeck::CardDeck()
{
	for (int suit = HEARTS; suit <= SPADES; ++suit)
	{
		for (int rank = ACE; rank <= KING; ++rank)
		{
			cards.push_back(new PlayingCard(rank, suit));
		}
	}
	srand((unsigned int)time(nullptr));	// Seed random number from the clock
}

CardDeck::CardDeck(const CardDeck& other)
{
	for (auto card : other.cards)
		cards.push_back(new PlayingCard(*card));
}

CardDeck::~CardDeck()
{
	for (auto card : cards)
		delete card;
	cards.clear();
	cout << "Delete the cards\n";
}

PlayingCard* CardDeck::deal()
{
	if (cards.empty())
		throw "No cards in deck";
	PlayingCard* retval = cards.back();
	cards.pop_back();
	return retval;
}

void CardDeck::add(PlayingCard* card)
{
	if (!card->is_face_up())
		card->flip();

	cards.push_back(card);
}


// Shuffle provided for you.

void CardDeck::shuffle()
{
	random_shuffle(cards.begin(), cards.end());
}

// Print provided for you

void CardDeck::print() const
{
	int i = 0;
	for (auto card : cards)
	{
		if (((i % 13) == 0) && (i != 0)) cout << '\n';
		card->print();
		++i;
	}
	cout << "\n";
}
