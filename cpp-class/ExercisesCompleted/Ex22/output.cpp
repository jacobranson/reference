#include <iostream>
#include <string>

void output(double euro)
{
	std::cout << "Converted is: " << euro << "\n";	// Output the result.
}

// for the bonus
void output(std::string currency, double euro)
{
	std::cout << currency << ": " << euro << "\n";	// Output the result.
}
