// Moneycon.cpp : Defines the entry point for the console application.
#include <iostream>
#include "get_amount.h"
#include "output.h"

int main()
{
	// conversion rates:
	//   USD 1.3033
	//   CAD 1.4968
	//   JPY 135.1
	//   SEK 9.528
	//   GBP 0.7382

	double amount = get_amount();
	std::cout << "The number of euros is: " << amount << "\n";

	// TODO: create an array of double to hold the conversion rates
	double rates[] {1.3033, 1.4968, 135.1, 9.528, 0.7382};

	std::cout << "\nThe converted values using a range-based for:\n";
	// TODO: use a range-based for loop to display the converted the input 
	//		 amount to euros and display the converted values
	for (auto rate : rates)
		output(amount * rate);

	return 0;
}
