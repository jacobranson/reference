const int HEARTS   = 0;
const int DIAMONDS = 1;
const int CLUBS	   = 2;
const int SPADES   = 3;

const int JOKER =  0;
const int ACE   =  1;
const int JACK  = 11;
const int QUEEN = 12;
const int KING  = 13;

class PlayingCard
{
public:
	PlayingCard(int r, int s);
	void print();
	void flip();
	int get_rank();
	int get_suit();
	bool is_face_up();
private:
	int rank;
	int suit;
	bool face_up;
};
