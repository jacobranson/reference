#include <iostream>
#include "playingcard.h"
using namespace std;

// Uncomment lines from the main program as directed in the exercise manual

int main()
{
	PlayingCard c1(3, HEARTS);		// Create card using default constructor..
	cout << "The card should report ##\n";
	c1.print();						// Print out c1.
	c1.flip();						// Now flip the card
	cout << "Now it should report 30\n";
	c1.print();

	PlayingCard c2(KING, CLUBS);
	cout << "The program should report the card is face up = 0 (false)\n";
	cout << "The card is face up = " << c2.is_face_up() << "\n";
	cout << "The program should report the card's rank is 13 and the card's suit is 2\n";
	cout << "The card's rank is " << c2.get_rank();
	cout << " and the card's suit is " << c2.get_suit() << "\n";

	return 0;
}
