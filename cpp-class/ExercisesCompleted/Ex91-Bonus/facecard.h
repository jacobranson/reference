#ifndef FACECARD_INCLUDED
#define FACECARD_INCLUDED
#include "playingcard.h"

class FaceCard : public PlayingCard
{
public:
	FaceCard(int r, int s);
	virtual void print() const override;
	PlayingCard* clone() const;
private:
	int eyes = 2;
};

#endif
