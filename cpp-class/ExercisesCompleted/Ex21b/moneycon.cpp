#include <iostream>
#include "get_dollars.h"
#include "convert.h"
#include "output.h"

int main()									// Entry point (called by system)
{
	auto dollar = get_dollars(100.0);		// Get a dollar amount.
	double euro = convert(dollar);			// Convert to euros.

	output(dollar, euro);

	return 0;								// Return to the system.
}

