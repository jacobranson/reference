#include <iostream>
#include "output.h"

void output(double dollar, double euro)
{
	std::cout << "Using output functions\n";
	std::cout << dollar << " dollars is " << euro << " euros.\n";
}
