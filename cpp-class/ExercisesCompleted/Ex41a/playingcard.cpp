#include <iostream>
#include "playingcard.h"
using namespace std;

// Place your definition for PlayingCard::PlayingCard() here
PlayingCard::PlayingCard()
{
	rank = 0;
	suit = 0;
	face_up = false;
}

// Place your definition for PlayingCard::print() here
void PlayingCard::print()
{
	if (face_up)
		cout << rank << suit << "\n";
	else
		cout << "##\n";
}

// Place your definition for PlayingCard::flip here
void PlayingCard::flip()
{
	face_up = !face_up;
}

int PlayingCard::get_rank()
{
	return rank;
}

int PlayingCard::get_suit()
{
	return suit;
}

bool PlayingCard::is_face_up()
{
	return face_up;
}

