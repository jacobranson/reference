#ifndef GAMEHAND_INCLUDED
#define GAMEHAND_INCLUDED

class PlayingCard;

class GameHand
{
public:
	GameHand() = default;
	void add(PlayingCard* cp);
	int evaluate() const;
	PlayingCard* get_highest_card() const;
	PlayingCard* get_lowest_card() const;
private:
	int evaluate_card(PlayingCard* cp) const;
	PlayingCard* cp1 = nullptr;
	PlayingCard* cp2 = nullptr;
};

#endif
