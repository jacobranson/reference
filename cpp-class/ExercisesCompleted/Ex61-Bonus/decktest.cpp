#include <iostream>
#include "carddeck.h"
#include "gamehand.h"
#include "PlayingCard.h"
using namespace std;

int main()
{
	try
	{
		CardDeck deck;			// Create a card deck
		cout << "Unshuffled...\n";
		deck.print();			// Print the deck
		deck.shuffle();			// Shuffle the deck
		cout << "Shuffled...\n";
		deck.print();			// Print it again

		GameHand hand;
		for (int i = 0; i < 2; i++)
		{
			PlayingCard* card = deck.deal();
			cout << "The card dealt is "; card->print(); cout << "\n";
			hand.add(card);
		}
		cout << "The hand's value is: " << hand.evaluate() << "\n";

		deck.add(hand.get_highest_card());
		deck.add(hand.get_lowest_card());

		for (int j = 0; j < 52; j++) // Note there are only 50 cards in deck
		{
			PlayingCard* card = deck.deal();
			cout << "The card dealt is "; card->print(); cout << "\n";
		}
	}
	catch(const char* msg)
	{
		cerr << msg << endl;
	}

	return 0;
}
