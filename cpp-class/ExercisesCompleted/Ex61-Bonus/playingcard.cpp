#include <iostream>
#include <string>
#include "playingcard.h"
using namespace std;

// Place your definition for PlayingCard::PlayingCard() here
PlayingCard::PlayingCard(int r, int s)
	: rank(r)
	, suit(s)
{}

// Place your definition for PlayingCard::print() here

void PlayingCard::print() const
{
	string rchars = " A23456789TJQK";
	if (face_up) cout << rchars[rank] << char(suit+3) << ' ';
	else cout << "## ";
}

// Place your definition for PlayingCard::flip here

void PlayingCard::flip()
{
	face_up = !face_up;
}

int PlayingCard::get_rank() const
{
	return rank;
}

int PlayingCard::get_suit() const
{
	return suit;
}

bool PlayingCard::is_face_up() const
{
	return face_up;
}
