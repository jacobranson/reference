#ifndef CARDDECK_INCLUDED
#define CARDDECK_INCLUDED

#include <deque>
class PlayingCard;

class CardDeck
{
public:
	CardDeck();
	CardDeck(const CardDeck&);
	~CardDeck();
	PlayingCard* deal();
	void add(PlayingCard* cp);
	void print() const;	// provided
	void shuffle();		// provided
private:
	std::deque<PlayingCard*> cards;
};

#endif
