// Moneycon.cpp : Defines the entry point for the console application.
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "get_amount.h"
using namespace std;

int main()
{
	double amount = get_amount();
	// TODO: create output file stream here
	ofstream fout("convert.txt");

	fout << "Converted is: " << amount << "\n";

	vector<double> rates = { 1.3033, 1.4968, 135.101, 9.528, 0.7382 };
	vector<string> codes = { "USD", "CAD", "JPY", "SEK", "GBP" };

	for (int i = 0; i < sizeof(rates) / sizeof(rates[0]); ++i)
		fout << codes[i] << ": " << amount * rates[i] << "\n";

	return 0;
}
