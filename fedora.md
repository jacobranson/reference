# Fedora Notes

## Firefox Configuration
Set the following values in `about:config`:
```bash
toolkit.legacyUserProfileCustomizations.stylesheets = true
widget.content.gtk-theme-override = Adwaita:light
full-screen-api.warning.timeout = 0
browser.in-content.dark-mode = true
ui.systemUsesDarkTheme = 1
```

## Installing Unity
1. Install .NET Core SDK
[here](https://docs.microsoft.com/en-us/dotnet/core/install/linux-package-manager-fedora31).
2. Install `mono-complete`.
3. Install `code` from Microsoft Repo (Flatpak has issues).
4. Install Unity Hub AppImage from their website (Flatpak has issues).
5. Set editor path in settings to be `~/.local/share/unity3d`.
6. Configure license.
7. Create a new project (Unity has a bug where adding a project doesn't work
until a new one is created.)
8. Close that project, then add the project you actually want to work on.
9. Delete the temporary project.
10. Install Unity version from Unity Hub.
11. Edit `/etc/environment` and add the following line:

```bash
# fix for mono version for vscode omnisharp c# plugin
FrameworkPathOverride="/lib/mono/4.5"
```

12. Reboot.
13. In Unity, go to Edit, Preferences, External Tools, and make sure the
external script editor says "Open by file extension". (For some reason, Unity
only generates project files on the default option in this list.)
14. In Unity, go to Assets, Open C# Project to generate project files.
15. Verify that the following project files have been generated in the root of
the project folder: `Assembly-CSharp.csproj`, `Assembly-CSharp-Editor.csproj`,
and a `.sln` file.
16. In Unity, go to Edit, Preferences, External Tools, and set the external
script editor to `/usr/bin/code`.
17. Install vscode extensions for `C#`, `Unity Debugger`, and `Live Share`.
18. Check the OmniSharp output window and verify the project was configured
correctly.

## Fixing Audio Crackling When Using Mic
1. Edit the file `/etc/pulse/default.pa`
2. Modify the following line as shown:

```bash
load-module module-udev-detect tsched=0
```

3. Run the following commands

```bash
$ pulseaudio --kill; pulseaudio --start
```

## Fixing Wired Xbox One S Controller Connection
1. Install `kernel-modules-extra` and reboot.

## Fixing QT Application Scaling
1. Edit the file `/etc/environment`
2. Add the following line:

```bash
export QT_AUTO_SCREEN_SCALE_FACTOR=1
```

## Grub Config
```bash
$ gksudo gedit /etc/default/grub
```
Make required modifications, then:
```bash
$ sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
```

## Scale GDM
```bash
$ gksudo gedit /usr/share/glib-2.0/schemas/org.gnome.desktop.interface.gschema.xml
```
Change "scaling-factor" to 2, then:
```bash
$ sudo glib-compile-schemas /usr/share/glib-2.0/schemas
```

## Disable File Indexing
```bash
systemctl --user mask \
tracker-store.service \
tracker-miner-fs.service \
tracker-miner-rss.service \
tracker-extract.service \
tracker-miner-apps.service \
tracker-writeback.service
```

## Configuring VPN Killswitch
1. Install and enable ufw
```bash
$ sudo dnf install ufw
$ sudo systemctl enable ufw
$ sudo systemctl start ufw
```
2. Deny all incoming and outgoing traffic by default
```bash
$ sudo ufw default deny outgoing
$ sudo ufw default deny incoming
```
3. Make an exception for VPN traffic
```bash
$ sudo ufw allow out on tun0 from any to any
$ sudo ufw allow in on tun0 from any to any
```
4. Find the IP Address of the VPN in the .ovpn file
5. Allow for the initial connection to the VPN
```bash
$ sudo ufw allow out from any to <ip_add>
```
6. Start the firewall
```bash
$ sudo ufw enable
```

## Finding Missing Libraries
```bash
$ repoquery -f '*filename'
```
### On Ubuntu
```bash
$ apt-file search 'filename'
```