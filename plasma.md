# Plasma Notes

## Fixing Bluetooth
1. In, System Settings, Multimedia, Audio Volume, Advanced, Change "Profile" to "High Fidelity Playback (A2DP Sink)".
2. In the same tab, check "Automatically switch all running streams when a new output device becomes available".
3. Applications may need to be manually changed. In the Applications tab, while audio is playing, change audio device if needed.

## Show Application Title in Titlebar
1. Download this plasmoid: https://gitlab.com/aleixq/apptitle-plasmoid
2. Extract and Make
2. Unlock the desktop (ALT+d, l)
3. Open a panel context menu (3 dots)
4. Click "Add Widgets..."
5. Click "Get new widgets"
6. Click "Install Widget From Local File..."
7. Select the Plasmoid File
8. Position as necessary

## Switching to SDDM
1. Install the following packages: sddm sddm-kcm
2. Run the following command:

```bash
sudo systemctl -f enable sddm
```

## Fixing HiDPI with SDDM
1. Edit the file /usr/share/sddm/scripts/Xsetup
2. Add the following line:

```bash
xrandr --dpi 192
```

## Fixing the Disabled Buttons on SDDM
1. Edit the file /lib/systemd/system/sddm.service
2. Add systemd-logind.service to the line "After="

## Fixing Markdown file previews in Okular
1. Install the "discount" package
2. Restart Okular

## Enabling Kate Previews
1. Go to Settings, Configure Kate, Plugins, Enable "Document Preview"

## Typing Special Characters with the Compose Key
1. Go to Settings, Hardware, Input Devices, Keyboard, Advanced
2. Check "Configure keyboard options"
3. Expand "Position of Compose key" and select desired key
4. Hold the assigned Compose key while entering values [here](https://en.wikipedia.org/wiki/Compose_key#Common_compose_combinations) to get the corresponding values

## Fixing icon theme errors with Papirus (and other icon themes)
1. Edit the file /usr/share/icons/<theme>/index.theme
2. Remove everything after "Inherits="

## Installing NordVPN
1. Install the "openvpn" package
2. Run the following commands:

```bash
$ cd /etc/openvpn
$ sudo wget "https://nordvpn.com/api/files/zip"
$ sudo unzip zip
$ sudo rm zip
```

3. Right click the network manager tray icon
4. Click "Configure Network Connections"
5. Add a new network connection by clicking the plus
6. Import VPN connection
7. Navigate to /etc/openvpn
8. Open https://nordvpn.com/servers/ in a web browser.
9. Click "Recommended Server"
10. Find that server in the servers list and select it
11. NOTE: UDP servers are default. They are fast but unstable. TCP servers are stable but slow.
12. Select the server and name it
13. Enter NordVPN account information (email and password)
14. NOTE: "Store password for this user only" requires Kwallet to be enabled, otherwise it won't save it.
15. NOTE: "Store password for all users" does not require Kwallet to be enabled.
16. Connect to VPN using the tray applet when necessary. VPN can be tested on https://nordvpn.com/
17. The header will say "protected" if VPN is enabled

## Settings
* Application Style
    * Window Decorations
        * Organize title bar
* Desktop Behavior
    * Screen Edges
        * Disable top-left corner
    * Screen Locking
        * Disable lock screen automatically
    * Virtual Desktops
        * Number of Desktops: 4
        * Number of Rows: 2
        * Disable desktop navigation wraps around
* Window Management
    * Window Behavior
        * Focus Stealing Prevention: None
* Startup and Shutdown
    * Start with an empty session
* Account Details
    * KDE Wallet
        * Disable KDE Wallet Subsystem
* Applications
    * Launch Feedback
        * Change bouncing cursor to no busy cursor
        * Disable Taskbar Notification
    * File Associations
        * Search "html"
        * On text, html, move Chrome to top
        * Same with application xhtml+xml
* Input Devices
    * Touchpad
        * Swap two and three finger taps
        * Disable tap to drag
        * Reverse scroll direction

